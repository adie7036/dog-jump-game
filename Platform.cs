using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    private SpriteRenderer platformSprite;

    private PlatformManager platformManager;    // Reference to the Platform Manager
    private int spriteSize;                     // The size of platform sprite
    private int bounces;                        // Number of Bounces on this Platform
    
    // All set in the Unity Inspector:
    public AudioSource bounceSound;     // Audio for a Bounce
    public AudioSource breakSound;      // Audio when a platform breaks

    // Each platform Size has 3 Sprites, Full, Half Broken, Very Broken, in that order
    public Sprite[] smallPlatformSprites;   // Sprites for Small Platform
    public Sprite[] normalPlatformSprites;  // Sprites for Normal Platform

    void Awake()
    {
        platformSprite = gameObject.GetComponent<SpriteRenderer>();
    }

    public void SetPlatformManager(PlatformManager platformManager)
    {
        this.platformManager = platformManager;
    }

    public void SetSprite()
    {
        switch (spriteSize)
        {
            case 0: // Small Sprites
                if (bounces < smallPlatformSprites.Length)
                    platformSprite.sprite = smallPlatformSprites[bounces];
                break;

            case 1: // Normal Sprites
            default:
                if (bounces < normalPlatformSprites.Length)
                    platformSprite.sprite = normalPlatformSprites[bounces];
                break;
        }
    }

    public void SetSize(int size)
    {
        spriteSize = size;
        bounces = 0;

        SetSprite();
    }

    public void SetBounces(int bounces)
    {
        this.bounces = bounces;

        SetSprite();
    }

    public float LeftEdge()
    {
        return platformSprite.bounds.min.x;
    }

    public float RightEdge()
    {
        return platformSprite.bounds.max.x;
    }

    public float Width()
    {
        return Math.Abs(RightEdge() - LeftEdge());
    }

    public Vector3 CalculateBounceForce(Collision2D collision)
    {
        // What is the x-axis position of the player (the center/pivot point)
        float playerXPos = collision.gameObject.transform.position.x;

        // What is the x-axis position of the platform (the center/pivot point)
        float platformXPos = this.transform.position.x;

        // How far is the player from the center of the platform
        float distFromCenter = Math.Abs(playerXPos - platformXPos);

        // How wide is the platform
        float platformWidth = Width();

        // Calculate the force
        // Force is 100% in the middle reducing to 80% at the far edges
        float force = 200.0f - (80f * (distFromCenter / platformWidth));

        return (Vector3.up * force);
    }

    private void HandlePlayerCollision(Collision2D collision)
    {
        // Recycle platform if it has been bounced too many times 
        if (bounces >= 2)
        {
            platformManager.RecycleThisPlatform(gameObject);
            breakSound.Play();
        }
        else
        {
            // Increment the Platforms number of Bounces
            bounces++;

            SetSprite();

            // Bounce the Player Up
            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(CalculateBounceForce(collision));
            bounceSound.Play();
        }

    }

    // Start is called before the first frame update
    void Start()
    {
          
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<Rigidbody2D>().velocity.y <= 0.0f)
        {
            HandlePlayerCollision(collision);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        // The Player may get stuck if "OnCollisionEnter2D" occurs and the velocity is
        // just larger than zero. This means the "HandlePlayerCollision" does not get called
        // If this occurs, then the collision will "Stay" and eventually
        // the Player velocity will reach exactly 0.0f

        if (collision.gameObject.GetComponent<Rigidbody2D>().velocity.y == 0.0f)
        {
            HandlePlayerCollision(collision);
        }
    }
}
