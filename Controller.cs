using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private SpriteRenderer playerSprite;

    private float speed = 10.0f; // Used to determine how fast the player moves left or right
    
    private float screenLeft;  // x value of the furthest left of the screen
    private float screenRight; // x value of the furthest right of the screen

    public GameOverScreen gameOverScreen;

    // Calculate the World Cordinates of the Left and Right sides of the Screen
    private void CalculateXAtSidesOfScreen()
    {
        // create a variable to access the main camera (tagged MainCamera in Unity)
        var camera = Camera.main;

        // Calculate the distance from the Camera to the Z-plane that the player is in:
        
        // z-axis value of the camera
        float zPlaneOfCamera = camera.transform.position.z;
        // z-axis value of the player
        float zPlaneOfPlayer = transform.position.z;
        // Distance between the player and the camera (and make it positive)
        float viewPortDistance = Mathf.Abs(zPlaneOfCamera - zPlaneOfPlayer);

        // Use ViewportToWorldPoint to find World View Co-Ordinates of the screen corners
        Vector3 screenBottomLeft = camera.ViewportToWorldPoint(new Vector3(0, 0, viewPortDistance));
        Vector3 screenTopRight = camera.ViewportToWorldPoint(new Vector3(1, 1, viewPortDistance));
        
        screenLeft = screenBottomLeft.x;
        screenRight = screenTopRight.x;
    }

    // Returns the World Coordinate of the Bottom of the Screen
    private float YAtBottomOfScreen()
    {
        // create a variable to access the main camera (tagged MainCamera in Unity)
        var camera = Camera.main;

        // Calculate the distance from the Camera to the Z-plane that the player is in:

        // z-axis value of the camera
        float zPlaneOfCamera = camera.transform.position.z;
        // z-axis value of the player
        float zPlaneOfPlayer = transform.position.z;
        // Distance between the player and the camera (and make it positive)
        float viewPortDistance = Mathf.Abs(zPlaneOfCamera - zPlaneOfPlayer);

        // Use ViewportToWorldPoint to find World View Co-Ordinates of the screen corners
        Vector3 screenBottomLeft = camera.ViewportToWorldPoint(new Vector3(0, 0, viewPortDistance));
        Vector3 screenTopRight = camera.ViewportToWorldPoint(new Vector3(1, 1, viewPortDistance));

        return screenBottomLeft.y;
    }

    private void UpdateScore()
    {
        // The score is calculated with the y position of the player
        float yPlayerPos = transform.position.y;
        int newScore = (int)Math.Round(yPlayerPos) * 10; 

        // If the new score is larger than the current score, update current score
        if (newScore > GameData.score)
            GameData.score = newScore;
    }

    // Awake is called before any Start methods, even if the GameObject is disabled
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();

        playerSprite = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update, if the GameObject is enabled
    void Start()
    {
        CalculateXAtSidesOfScreen();
    }

    // FixedUpdate is called on each fixed framerate, if the GameObject is enabled
    // Use instead of Update for a RigidBody
    void FixedUpdate()
    {
        UpdateScore();

        // User's horizontal movement input
        float moveInput = Input.GetAxis("Horizontal");

        // Calculate Players horizontal velocity
        // Velocity is measured in Real World Units per Second
        float xVelocity = moveInput * speed;

        // Find the Sprite's Left and Right Edges in World Coordinates
        float spriteLeftEdge = playerSprite.bounds.min.x;
        float spriteRightEdge = playerSprite.bounds.max.x;

        // Player wants to move Left
        if (xVelocity < 0)
        {
            // Distance from Player Sprite's Left edge to Left of Screen ( should be negative ) 
            float distanceToLeftScreen = screenLeft - spriteLeftEdge;

            // Ensure Velocity does not move Sprite off screen
            xVelocity = Mathf.Max(xVelocity, distanceToLeftScreen / Time.fixedDeltaTime);
        }

        // Player wants to move right
        if (xVelocity > 0)
        {
            // Distance from Player Sprite's Right Edge to Right of Screen ( should be postive )
            float distanceToRightScreen = screenRight - spriteRightEdge;

            // Ensure Velocity does not move Sprite off screen
            xVelocity = Mathf.Min(xVelocity, distanceToRightScreen / Time.fixedDeltaTime);
        }

        // Set the velocity of the rigidbody2D that is attatched to the player Game Object
        rb2d.velocity = new Vector2(xVelocity, rb2d.velocity.y);

        float spriteTopEdge = playerSprite.bounds.max.y;
        if (spriteTopEdge < YAtBottomOfScreen())
        {
            // Update High Scores
            if (GameData.score > PlayerPrefs.GetInt("highscore"))
                PlayerPrefs.SetInt("highscore", GameData.score);

            SceneManager.LoadScene("GameOver");
        }
    }

    // Update is called once per frame, if the GameObject is enabled
    void Update()
    {

    }
}
