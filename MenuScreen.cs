using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuScreen : MonoBehaviour
{
    public AudioSource audioSource;

    void Awake()
    {
        audioSource.Play();
    }

    // Loads the game scene 
    public void PlayButton()
    {
        GameData.score = 0;
        SceneManager.LoadScene("Game");
    }

    public void ExitButton()
    {
        Application.Quit();
    }
}
