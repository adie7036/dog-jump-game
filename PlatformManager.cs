using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformManager : MonoBehaviour
{
    public GameObject platformObject;

    private float screenLeft; // x value of the furthest left of the screen
    private float screenRight; // x value of the furthest right of the screen

    // List<GameObject> defines referrence to a list that can hold GameObjects
    // platforms is name of the variable

    // The First Item in the List is the Lowest (y-axis) Platform
    // The Last Item in the List is the Highest (y-axis) Platform
    private List<GameObject> platforms;

    // Calculate the World Cordinates of the Left and Right sides of the Screen
    private void CalculateXAtSidesOfScreen()
    {
        // Create a variable to access the main camera (tagged MainCamera in Unity)
        var camera = Camera.main;

        // Calculate the distance from the Camera to the Z-plane that the player is in:

        // z-axis value of the camera
        float zPlaneOfCamera = camera.transform.position.z;
        // z-axis value of the player
        float zPlaneOfPlayer = transform.position.z;
        // Distance between the player and the camera (and make it positive)
        float viewPortDistance = Mathf.Abs(zPlaneOfCamera - zPlaneOfPlayer);

        // Use ViewportToWorldPoint to find World View Co-Ordinates of the screen corners
        Vector3 screenBottomLeft = camera.ViewportToWorldPoint(new Vector3(0, 0, viewPortDistance));
        Vector3 screenTopRight = camera.ViewportToWorldPoint(new Vector3(1, 1, viewPortDistance));

        screenLeft = screenBottomLeft.x;
        screenRight = screenTopRight.x;
    }

    // Returns the World Coordinate of the Bottom of the Screen
    private float YAtBottomOfScreen()
    {
        // create a variable to access the main camera (tagged MainCamera in Unity)
        var camera = Camera.main;

        // Calculate the distance from the Camera to the Z-plane that the platforms are in
        float zPlaneOfCamera = camera.transform.position.z;
        float zPlaneOfPlatforms = platformObject.transform.position.z;
        float viewPortDistance = Mathf.Abs(zPlaneOfCamera - zPlaneOfPlatforms);

        // Use ViewportToWorldPoint to find World View Co-Ordinates of the screen corners
        Vector3 screenBottomLeft = camera.ViewportToWorldPoint(new Vector3(0, 0, viewPortDistance));
        Vector3 screenTopRight = camera.ViewportToWorldPoint(new Vector3(1, 1, viewPortDistance));

        return screenBottomLeft.y;
    }

    private float YOfHighestPlatform()
    {
        if (platforms.Count <= 0)
            return 0.0f;

        // Get the Highest Platform
        GameObject highestPlatform = platforms[platforms.Count - 1];

        // Y Position of Highest Platform
        return highestPlatform.transform.position.y;
    }

    private Vector3 CalculatePlatformPosition(GameObject platform, Platform platformScript)
    {
        float x; 
        float y;

        // Is this the first Platform ?
        if (this.platforms.Count <= 0)
            return (new Vector3(0.0f, -3.0f, 0.0f));

        // Y Position of Highest Platform
        y = YOfHighestPlatform();

        // Next Platform will be above the current Highest Platform
        y += Random.Range(0.4f, 2.2f);

        // Randomly Choose X Position
        x = Random.Range(screenLeft + platformScript.Width(), screenRight - platformScript.Width());

        // Keep Platform in the same Z Plane
        Vector3 nextPlatformPosition = new Vector3(x, y, platform.transform.position.z);

        return (nextPlatformPosition);
    }

    // Initialise the Platform (set the size, bounces and position)
    private void InitialisePlatform(GameObject platform)
    {
        // Get the Platform Script from platform GameObject
        Platform platformScript = platform.GetComponent<Platform>();

        // Set the Platforms reference to the PlatformManager
        platformScript.SetPlatformManager(this);

        // y-axis position of the current Highest Platform
        float yOfHighestPlatform = YOfHighestPlatform();

        // Calculate the size of the platform
        // Increase the probablity of having a smaller platform the higher you go
        float probOfSmall;

        if (yOfHighestPlatform > 70)
            probOfSmall = 0.8f;
        else if (yOfHighestPlatform > 50)
            probOfSmall = 0.6f;
        else if (yOfHighestPlatform > 30)
            probOfSmall = 0.4f;
        else if (yOfHighestPlatform > 10)
            probOfSmall = 0.2f;
        else
            probOfSmall = 0.0f;

        if (Random.Range(0.0f, 1.0f) <= probOfSmall)
            platformScript.SetSize(0);
        else
            platformScript.SetSize(1);

        // Calcuate the platforms "bounce"
        // Increase the probablity of having a broken platform the higher you go
        float probOfHalfBroken;

        if (yOfHighestPlatform > 80)
            probOfHalfBroken = 0.8f;
        else if (yOfHighestPlatform > 60)
            probOfHalfBroken = 0.6f;
        else if (yOfHighestPlatform > 40)
            probOfHalfBroken = 0.4f;
        else if (yOfHighestPlatform > 20)
            probOfHalfBroken = 0.2f;
        else
            probOfHalfBroken = 0.0f;

        if (Random.Range(0.0f, 1.0f) <= probOfHalfBroken)
            platformScript.SetBounces(1);
        else
            platformScript.SetBounces(0);

        // Calculate Position for the platform, need to set Platform Size before doing this
        Vector3 position = CalculatePlatformPosition(platform, platformScript);
        platform.transform.position = position;
    }

    private void CreatePlatforms()
    {
        for (int i = 0; i < 16; i++)
        {
            // Create Platform Game Object
            GameObject platform = Instantiate(platformObject, Vector3.zero, Quaternion.identity);

            InitialisePlatform(platform);

            this.platforms.Add(platform);
        }
    }

    private void RecyclePlatformAtIndex(int index)
    {
        // Get the platform we are recycling at the specified index
        GameObject platform = platforms[index]; 

        // Remove the platform from the list
        this.platforms.RemoveAt(index);

        // Initalise Platform
        InitialisePlatform(platform);

        // Add the platform to the back of the list
        this.platforms.Add(platform);
    }

    public void RecycleThisPlatform(GameObject platform)
    {
        try
        {
            int index = platforms.IndexOf(platform);
            RecyclePlatformAtIndex(index);
        }
        catch (System.Exception exception)
        {
            Debug.Log("Error, did not find the platform: " + exception.Message);
        }
    }

    // Recycle Platforms
    // Move platforms that have off the bottom of the screen to the top
    private void RecyclePlatforms()
    {
        int count = 0;
        do
        {
            // The First Item in the List has the Lowest y-axis position
            GameObject platform = this.platforms[0];

            // Is the platform off the screen
            if (platform.transform.position.y < this.YAtBottomOfScreen())
            {
                RecyclePlatformAtIndex(0);

                // How many platforms have we recycled ?
                count++;
            }
            else
            {
                // No platform to recycle, break loop
                break;
            }

        } while (count < this.platforms.Count);
    }

    // Awake is called before any Start methods, even if the GameObject is disabled
    void Awake()
    {
        // Create a new List<GameObject> Object and assign the reference to platforms
        this.platforms = new List<GameObject>();
    }

    // Start is called before the first frame update, if the GameObject is enabled
    void Start()
    {
        CalculateXAtSidesOfScreen();

        CreatePlatforms();
    }

    // FixedUpdate is called on each fixed framerate, if the GameObject is enabled
    // Use instead of Update for a RigidBody
    void FixedUpdate()
    {
        RecyclePlatforms();
    }

    // Update is called once per frame, if the GameObject is enabled
    void Update()
    {

    }
}