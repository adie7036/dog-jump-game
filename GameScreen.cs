using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameScreen : MonoBehaviour
{
    public Text scoreText;
    public AudioSource audioSource;

    void Awake()
    {
        audioSource.Play();
    }

    // FixedUpdate is called on each fixed framerate, if the GameObject is enabled
    void FixedUpdate()
    {
        scoreText.text = "SCORE: " + GameData.score;
    }

}
