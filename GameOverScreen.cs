using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverScreen : MonoBehaviour
{
    public Text scoreText;
    public Text highScoreText;

    public AudioSource audioSource;

    void Awake()
    {
        audioSource.Play();
    }

    // Start is called before the first frame update, if the GameObject is enabled
    void Start()
    {
        scoreText.text = "SCORE: " + GameData.score;
        highScoreText.text = "HIGH SCORE: " + PlayerPrefs.GetInt("highscore");
    }

    public void PlayButton()
    {
        GameData.score = 0;
        SceneManager.LoadScene("Game");
    }

    public void HomeButton()
    {
        SceneManager.LoadScene("Menu");
    }

    public void ExitButton()
    {
        Application.Quit();
    }
}
