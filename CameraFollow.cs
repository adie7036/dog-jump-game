using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform player; // reference to the transform of the player

    // LateUpdate is called after all Update functions have been called 
    // A follow camera should always be implemented in LateUpdate because
    // it tracks objects that might have moved inside Update.
    private void LateUpdate()
    {
        // transform.position is the co-ordinates of the camera
        // if the player position is higher (y-axis) than the position of the camera,
        // then move the camera up in the y-axis to match the y-axis of where the player is
        if (player.position.y > transform.position.y)
        {
            Vector3 newPosition = new Vector3(transform.position.x, player.position.y, transform.position.z);
            transform.position = newPosition;
        }
    }
}
